using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using GameResourse;
using TMPro;

public class ProductionBuilding : MonoBehaviour
{
    [SerializeField] private ProductionTime _productionTime;
    [SerializeField] private ResourceBunk _resourceBank;
    [SerializeField] private Slider _mySlider;
    [SerializeField] private GameResource Res;

    private void Start()
    {
        _mySlider.interactable = false;
    }
    public void Add()
    {
        _mySlider.interactable=true;
        _productionTime.OnCorut();
    }

    public void AddMainComponent()
    {
        _mySlider.interactable = false;
        _resourceBank.ChangeResource( Res, 1);
    }
}
