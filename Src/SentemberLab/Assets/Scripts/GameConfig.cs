using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "myBunk", fileName = "Box")]
public class GameConfig : ScriptableObject
{
    [Range(0, 100)]
    public int Humans;

    [Range(0, 100)]
    public int Food;

    [Range(0, 100)]
    public int Wood;

    [Range(0, 100)]
    public int Stone;

    [Range(0, 100)]
    public int Gold;
}
