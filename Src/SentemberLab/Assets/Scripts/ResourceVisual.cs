using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameResourse;
using TMPro;

public class ResourceVisual : MonoBehaviour
{
    [SerializeField] private GameResource _gameResourse;
    [SerializeField] private ResourceBunk _resourceBank;
    [SerializeField] private Game _game;
    [SerializeField] private TextMeshProUGUI _text;

    void Start()
    {
        _text.text = _resourceBank.GetResource(_gameResourse).ToString();
    }

    public void UpdateText(int x)
    {
        _text.text = $"{x.ToString()}";
    }
}
