using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class ProductionTime : MonoBehaviour
{
    [SerializeField] private ProductionBuilding _productionBuilding;

    [SerializeField] private Button _myButton;
    [SerializeField] private Slider _mySlider;

    public void OnCorut()
    {
        StartCoroutine("OffButton");
    }

    private IEnumerator OffButton()
    {
        _myButton.interactable = false;
        _mySlider.interactable = true;

        _mySlider.value = 0;

        while (true)
        {
            
            _mySlider.value += 1;
            if (_mySlider.value == _mySlider.maxValue)
            {
                Debug.Log("� if");
                _myButton.interactable = true;
                _productionBuilding.AddMainComponent();
                _mySlider.value = 0;
                StopCoroutine("OffButton");
            }
            yield return new WaitForSeconds(0.1F);
        }
    }
    
}
