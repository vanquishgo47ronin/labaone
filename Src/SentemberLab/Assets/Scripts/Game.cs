using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Game : MonoBehaviour
{
    [SerializeField] public GameConfig _gameConfig;
    private void Awake()
    {
        _gameConfig.Humans = 10;
        _gameConfig.Food = 5;
        _gameConfig.Wood = 5;
    }
}
